import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { AppComponent } from "./app.component";
import { KruskeView } from "./routes/KruskeView";
import { JednaKruska } from "./routes/JednaKruska";
import { HomeView } from "./routes/HomeView";
import { Menu } from "./components/menu";
import { Edit } from "./routes/jednakruska/edit";
import { Info } from "./routes/jednakruska/info";
import { KruskeService } from "./services/kruske.services";
import { KruskeModel } from "./services/kruske.model";
import { HttpModule } from "@angular/http";
import { Delete } from "./routes/jednakruska/delete";
import { NewKruska } from "./routes/newkruska";
import { FilterPipe } from "../pipes/searchFilter";

const routes: Routes = [
  { path: "", component: HomeView },
  { path: "kruske", component: KruskeView },
  { path:"kruske/new", component:NewKruska},
  {
    path: "kruske/:id",component: JednaKruska,
    children: [
      { path: "", redirectTo:'info', pathMatch:'full' },
      { path: "edit", component:Edit },
      { path: "info", component:Info },
      { path: "delete", component:Delete }
    ]
  },
  { path: "**", redirectTo: "/" }
];

@NgModule({
  declarations: [
    AppComponent,
    KruskeView,
    JednaKruska,
    HomeView,
    Menu,
    Edit,
    Info,
    Delete,
    NewKruska,
    FilterPipe
  ],
  imports: [BrowserModule, RouterModule.forRoot(routes), HttpModule,FormsModule],
  providers: [KruskeService,KruskeModel],
  bootstrap: [AppComponent]
})
export class AppModule {}
