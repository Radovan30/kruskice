import { Component } from "@angular/core";

@Component({
  selector: "menu-list",
  templateUrl: "./menu.html"
})
export class Menu {
  links = [
    { naziv: "Home", putanja: "/", jesamLiAktivan: false },
    { naziv: "Kruske", putanja: "/kruske", jesamLiAktivan: false },
    { naziv: "Dodaj jabuku", putanja: "/kruske/new", jesamLiAktivan: false }
  ];
}
