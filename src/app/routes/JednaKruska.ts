import { Component } from '@angular/core';
import{ActivatedRoute} from '@angular/router'
@Component({
    templateUrl: './jednakruska.html'
})
export class JednaKruska {
    id;
    constructor(private route:ActivatedRoute){
        this.route.params.subscribe((paramsObject)=>{
            this.id = paramsObject['id'];
        })
    }

}