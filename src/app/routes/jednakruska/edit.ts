import { Component, OnInit } from "@angular/core";
import { colors } from "../../configs/colors";
import { KruskeModel } from "../../services/kruske.model";
import { RouterModule, ActivatedRoute, Router } from "@angular/router";
@Component({
  templateUrl: "./edit.html"
})
export class Edit implements OnInit {
  constructor(
    private kruskeModel: KruskeModel,
    private route: ActivatedRoute, private router:Router
  ) {}
  kruske = [];

  colors = colors;
  id;

  ngOnInit() {
    this.route.parent.params.subscribe(params => (this.id = params.id));
    this.kruskeModel.getKruska(this.id);
    
    console.log(this.id);
  }

  /* editKruska() {
      console.log(this.kruskeModel.kruska);
      
    this.kruskeModel.editKruske(this.kruskeModel.kruska);
    this.router.navigate(['/kruske']);
  } */
  editKruska(id){
    this.kruskeModel.editKruske(this.kruskeModel.kruska,()=>{
      this.router.navigate(['/kruske']);
    });
    
  }
}
