import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { KruskeModel } from "../../services/kruske.model";

@Component({
  templateUrl: "./delete.html"
})
export class Delete implements OnInit {
  id;
  constructor(private route: ActivatedRoute, private kruskeModel:KruskeModel, private router:Router) {}
  ngOnInit() {
    this.route.parent.params.subscribe(paramas => {
      this.id = paramas.id;
    });
  }

  deleteKrusku(id){
    this.kruskeModel.deleteKrusku(this.id,()=>{
      this.router.navigate(['/kruske']);
    });
    
  }

 
}
