import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    templateUrl: './info.html'
})
export class Info implements OnInit {
    id;
    constructor(private route:Router, private aroute:ActivatedRoute){}
    ngOnInit(){
        this.aroute.parent.params.subscribe((paramas) => {
            this.id = paramas.id;
          })
    };
}