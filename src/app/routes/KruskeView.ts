import { Component, Input,Output, EventEmitter, } from "@angular/core";
import { KruskeModel } from "../services/kruske.model";
import { Router } from "@angular/router";

@Component({
  templateUrl: "./kruske.html"
})
export class KruskeView {
  public searchString: string;
  constructor(private kruskeModel: KruskeModel, private router: Router) {}
  otvoriKrusku(id) {
    this.router.navigate(["/kruske", id]);
  }

  editKrusku(id) {
    this.router.navigate(["/kruske", id, "edit"]);
  }

  deleteKrusku(id) {
    this.router.navigate(["/kruske", id, "delete"]);
  }
}
