import { Component } from '@angular/core';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { KruskeModel } from '../services/kruske.model';
import { Router } from '@angular/router';
import { colors} from '../configs/colors';
@Component({
    templateUrl: './newkruska.html'
})
export class NewKruska {
    constructor(private kruskeModel:KruskeModel, private router:Router){};
  colors = colors;
kruske = {};

dodajKrusku(id){
    console.log(this.kruske);
    if(this.kruske['nutritionValue'] && this.kruske['weight']){
        this.kruskeModel.dodajKrusku(this.kruske,()=>{
            this.router.navigate(['/kruske/new']);
          });
    }

      };
};

