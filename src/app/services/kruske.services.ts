import { Injectable } from "@angular/core";
import {Http} from "@angular/http";
import 'rxjs/add/operator/map';
@Injectable()
export class KruskeService {

    constructor(private http:Http){}

    getKruskeObservable(){
        return this.http.get('http://localhost:3000/kruske')
        .map((response)=>{
            return response.json();
        });
    }

    getJednaKruskaObservable(id){
        return this.http.get('http://localhost:3000/kruske/'+id).map((response) => { return response.json()});
    }
    addKruskaObeservable(data){
        return this.http.post('http://localhost:3000/kruske',data).map((response) => { return response.json()});
    }
    updateKruskaObservable(data){
        console.log(data)
        return this.http.put('http://localhost:3000/kruske/'+ data.id, data).map((response) => {  response.json()});
        
    }
    deleteKruskaObservable(id){
        return this.http.delete('http://localhost:3000/kruske/'+id).map((response) => { return response.json()});
    }
}
