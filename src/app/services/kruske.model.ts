import { Injectable } from "@angular/core";
import { Kruska } from "../interfaces/kruska";
import { KruskeService } from "../services/kruske.services";
@Injectable()
export class KruskeModel {
  kruske = [];
  kruska ={};
  constructor(private service: KruskeService) {
    this.popuniKruske();
  }

  popuniKruske() {
    this.service.getKruskeObservable().subscribe(kruske => {
      this.kruske = kruske;
    });
  }
  getKruska(id){
    this.service.getJednaKruskaObservable(id).subscribe((kruska) => {this.kruska = kruska})
    
  }
  deleteKrusku(id, callbackFukcija) {
    this.service.deleteKruskaObservable(id).subscribe(() => {
      this.popuniKruske();
      callbackFukcija();
    });
  }

  dodajKrusku(kruska, callbackFukcija) {
    this.service.addKruskaObeservable(kruska).subscribe(() => {
      this.kruske.push(kruska);
    });
  }
  dajMiKruskuPoId(id) {
    for (var i = 0; i < this.kruske.length; i++) {
      if (this.kruske[i].id == id) {
        return this.kruske[i];
      }
    }
  }

  editKruske(data,callbackFukcija){
    this.service.updateKruskaObservable( data).subscribe(() => {
      this.popuniKruske();
      callbackFukcija();
    });
  }
}
