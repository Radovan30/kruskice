import { Pipe, PipeTransform, Injectable } from "@angular/core";

@Pipe({
  name: "filterSearch"
})
@Injectable()
export class FilterPipe implements PipeTransform {
  transform(items: any[], searchString: any): any[] {
    if (!items) {
      return [];
    }
    if (!searchString) {
      return items;
    }
    console.log(searchString);
   console.log(items);
   

    return items.filter(singleItem =>String(singleItem["nutritionValue"]).toLowerCase().includes(searchString.toLowerCase())
    );
  }
}
